﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSETWeb_Api.BusinessLogic.Models
{
    public class AnalyticsQuestionAnswer
    {
        public int QuestionId { get; set; }
        public string QuestionText { get; set; }
        public string Answer_Text { get; set; }
    }
}
